# AdminBundle

Add to **composer.json** file of **YourProject** :

    "repositories": [{
        "type": "vcs",
        "url": "http://192.168.1.179:8080/dev/adminbundle"
    }]
    
    
Still in **composer.json** file of **YourProject** add this line

    "secure-http": false  

to avoid the secure connection, like this :

    "config": {
        "preferred-install": {
            "*": "dist"
        },
        "sort-packages": true,
        "secure-http": false
    },
    
    
    
Then execute in term : 

    composer require -vvv emforbfc/adminbundle:dev-master@dev
      

Add these lines in **"YourProject/config/services.yaml"** :

    emforbfc\AdminBundle\Controller\CrudTrait:
        tags: ['controller.service_arguments']
        
    emforbfc\AdminBundle\Service\ContextService:
        tags: ['controller.service_arguments']
        
Il faut aller dans le dossier: 
**./vendor/emforbfc/adminbundle/src/Controller** et copier le fichier **CrudController.php** dans votre projet et ensuite le supprimer dans le bundle afin d'éviter les conflits.

La dernière étape consiste à créer les controller. Par exemple UtilisateurController : 

    /**
     * @Route("/utilisateur/")
     */
    class UtilisateurController extends CrudController
    {
        public const PAGE = 'utilisateur';
        public const ENTITY = Utilisateur::class;
        public const TYPE = UtilisateurType::class;
    
        /**
         * @var Environment
         */
        protected $environment;
    
        /**
         * @var object|string
         */
        protected $user;
    
        /**
         * Executé à l'appel du COntroller et AVEC AutoWiring :D
         * @required
         * @param Environment $environment
         * @param TokenStorageInterface $tokenStorage
         */
        public function initController(Environment $environment, TokenStorageInterface $tokenStorage)
        {
            $this->environment = $environment;
            $this->user = $tokenStorage->getToken()->getUser();
        }
    }
    
Il faut définir obligatoirement dans chaque controller les trois constantes: **PAGE / ENTITY / TYPE**.
Au lieu d'utiliser le **__construct** habituel il faut passer par **initController** avec l'annotation **'@required'** 
        
        
Une fois que tout est fait, il ne reste plus qu'à créer les Entités, les Forms et les Repository.