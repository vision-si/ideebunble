<?php

namespace emforbfc\AdminBundle\Resources\Form;

use App\Entity\Fonctionnalite;
use App\Entity\Profil;
use App\Entity\ProfilFonctionnalite;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateIntervalType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ResetType;

class FiltreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $session = new session();
        $nom=$session->get('nom');
        $fixe=$session->get('fixe');
        $ville=$session->get('ville');
        $portable=$session->get('portable');
        foreach ($options['filtres'] as $keyFiltre => $filtre) {
            switch ($filtre['type']){
                case 'select-multiple':
                    $builder
                        ->add($keyFiltre, ChoiceType::class, [
                            'choices' => $this->fillDataSelect($filtre, $keyFiltre, $options),
                            'multiple' => true,
                        ]);
                    break;

                case 'select':
                    $builder
                        ->add($keyFiltre, ChoiceType::class, [
                            'choices' => $this->fillDataSelect($filtre, $keyFiltre, $options),
                        ]);
                    break;

                case 'date_interval':
                    $builder
                        ->add($keyFiltre, RepeatedType::class, [
                            'type' => DateType::class,
                            'first_options'  => ['widget' => 'single_text', 'label' => 'A partir du'],
                            'first_name' => 0,

                            'second_options' => ['widget' => 'single_text', 'label' => "Jusqu'au"],
                            'second_name' => 1
                        ]);
                    break;
                case 'text':
                    if($keyFiltre=='nom' && isset($keyFiltre)){
                        $builder
                            ->add($keyFiltre, TextType::class,[
                                'data' =>$nom,
                                'required'=>false,
                            ]);
                    }
                    elseif($keyFiltre=='ville' && isset($keyFiltre)){
                        $builder
                            ->add($keyFiltre, TextType::class,[
                                'data' =>$ville,
                                'required'=>false,
                            ]);
                    }
                    elseif($keyFiltre=='tel_portable' && isset($keyFiltre)){
                        $builder
                            ->add($keyFiltre, TextType::class,[
                                'data' =>$portable,
                                'required'=>false,
                            ]);
                    }
                    elseif($keyFiltre=='tel_fixe' && isset($keyFiltre)){
                        $builder
                            ->add($keyFiltre, TextType::class,[
                                'data' =>$fixe,
                                'required'=>false,
                            ]);
                    }
                    else{
                        $builder
                            ->add($keyFiltre, TextType::class,);
                    }

                    break;
            }
        }
    }


    /**
     * @param $filtre
     * @param $keyFiltre
     * @param $option
     * @return array
     */
    public function fillDataSelect($filtre, $keyFiltre, $option) {
        $dataReturn = ['Tous' => ''];

        foreach ($option['dataForFiltres'][$keyFiltre] as $key => $data) {

            $dataSend = isset($filtre['dataSend']) ? $filtre['dataSend'] : 'value';
            $label = isset($filtre['label']) ? $filtre['label'] : 'label';

            if(isset($data[$dataSend])){
                $dataReturn += array($data[$label] => $data[$dataSend]);
            }
        }
        return $dataReturn;
    }

    public function getBlockPrefix()
    {
        return null;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'filtres' => null,
            'dataForFiltres' => null,
            'csrf_protection' => false,
            'attr' => ['id' => 'filtre', 'class'=>'row ', 'style'=>'padding-left: 15px;']
        ]);
    }
}