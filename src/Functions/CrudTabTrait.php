<?php


namespace emforbfc\AdminBundle\Functions;

use App\Service\CheckRuleActionService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Ce trait est utilisé afin de créer dynamiquement des onglets au sein de l'édition d'un élement.
 * (Par exemple: dans un élément de fonctionnalité nous pouvons avoir un onglet Actions.)
 *
 * Trait TabEditTrait
 * @package emforbfc\AdminBundle\Functions
 */
Trait CrudTabTrait
{
    /**
     * Cette fonction permet d'afficher la liste des éléments.
     *
     * @Route("edit/{id}/tab/{ongl}/", methods="GET|POST")
     * @param $id
     * @param $ongl
     * @param Request $request
     * @return mixed
     */
    final public function tabList($id, $ongl, Request $request, CheckRuleActionService $checkRuleActionService) {
        return $this->myTabList($id, $ongl, $request, $checkRuleActionService);
    }

    /**
     * Cette fonction permet d'ajouter et de modifier un élément.
     *
     * @Route("edit/{id}/tab/{ongl}/form/{id_tab}", methods="GET|POST")
     * @param Request $request
     * @param $ongl
     * @return Response
     * @throws Exception
     */
    final public function formTabList($id, $ongl, $id_tab=null, Request $request) {
        return $this->myFormTabList($id, $ongl, $id_tab, $request);
    }


    /**
     * Fonction de surcharge qui est appelée dans formTabList. Peut être surchargée entièrement dans un Controller.
     *
     * @param $id
     * @param $ongl
     * @param null $id_tab
     * @param $request
     * @return JsonResponse
     */
    public function myFormTabList($id, $ongl, $id_tab=null, $request) {
        $onglets = $this->getConfOnglets()->getConfArray();
        $jsonRep = new JsonResponse();

        if ((isset($onglets[$ongl]['entity']) && !empty($onglets[$ongl]['entity'])) && (isset($onglets[$ongl]['type']) && !empty($onglets[$ongl]['type']))) {
            $entityTabName = $onglets[$ongl]['entity'];
            $type = $onglets[$ongl]['type'];
            $thisEntity = isset($id_tab) ? $this->entityManager->getRepository($entityTabName)->find($id_tab) : new $entityTabName();
            $form = $this->createForm($type, $thisEntity, array(
                'action' => $this->generateUrl($request->get('_route'), ['id' => $id, 'id_tab' => $id_tab, 'ongl' => $ongl]),
                'id' => $id_tab,
            ));
            $form->handleRequest($request);
            $data = $form->getData();

            if($form->isSubmitted() && $form->isValid()) {
                if (!isset($id_tab)) {
                    $data->setUid();
                    $data->setCreeLe();
                    $this->setSpecificDataTab($data, $id, $id_tab);
                    $this->specificSetFunction($request, $this->entityManager, $data, true);
                    $this->addFlash("success", "Créé avec succès");
                } else {
                    $this->addFlash("success", "Modifié avec succès");
                }
                $this->entityManager->persist($data);
                $this->entityManager->flush();
                return $jsonRep->setData(['data' => 1]);
            }
        }

        return $this->render('@Admin/form-tab-list.html.twig', [
            'form' =>$form->createView(),
            'page' =>'',
        ]);
    }

    /**
     * Fonction qui permet de set des éléments spécifiques. Par exemple des relations OneToMany qui ne sont pas gérées automatiquement etc.. Doit être surchargée dans une controller.
     *
     * @param $entityTab
     * @param $id
     * @param $id_tab
     * @return null
     */
    public function setSpecificDataTab(&$entityTab, $id, $id_tab) {
        return null;
    }

    /**
     * Fonction de surcharge qui est appelée dans tabList. Peut être surchargée entièrement dans un Controller.
     *
     * @param $id
     * @param $ongl
     * @param $request
     * @return mixed
     */
    public function myTabList($id, $ongl, $request, $checkRule) {
        $getNameRoute = Utils::explodeNameRoute($request->attributes->get('_route'), static::PAGE);
        $data = [];
        $row = [];
        $getData = null;

        $all = $this->entityManager->getRepository(static::ENTITY)->find($id);
        $onglets = $this->getConfOnglets()->getConfArray();

        if (isset($onglets) && !empty($onglets)) {
            if (isset($onglets[$ongl]['name_fonction']) && !empty($onglets[$ongl]['name_fonction'])) {
                $val = strval($onglets[$ongl]['name_fonction']);
                $getData = $all->$val();


                if (isset($onglets[$ongl]['conf_col']) && !empty($onglets[$ongl]['conf_col'])){
                    foreach ($getData as $key => $result) {
                        
                        $entity = $result->toArray();
                        
                        foreach ($onglets[$ongl]['conf_col'] as $index => $onglet) {
                            $val = $this->getColumnValueTab($index, $entity, $onglet);

                            if (is_null($val)) {
                                switch ($index) {
                                    case 'actions':
                                        if ($checkRule->checkRuleFonctionnalite(strtoupper($this->entityShortName), 2)) {
                                            $val = '<a href="#" class="btn btn-primary btn-sm crudModal" data-url="'.$this->router->generate($getNameRoute.'_'.$this::PAGE.'_formtablist', ['id'=>$id ,'id_tab'=>$entity['id'], 'ongl'=>$ongl]).'" data-id="'.$entity['id'].'" data-callback="crudIhm.tabRefresh" data-title="Edition "><i class="fa fa-pencil"></i></a>
                                                <form id="deleteFormTab" class="formDeleteFormTab" method="post" action=\''.$this->router->generate($getNameRoute.'_'.$this::PAGE.'_deletetablist', ['id_tab' => $entity['id'], 'ongl' => $ongl]).'\' style="display: inline-block" data-callback="crudIhm.tabRefresh">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="'.$this->container->get('security.csrf.token_manager')->getToken('delete').'">
                                                    <button class="btn btn-sm btn-danger" id="deleteTab"><i class="fa fa-trash"></i></button>
                                                </form>';
                                        } else if ($checkRule->checkRuleFonctionnalite(strtoupper($this->entityShortName), 1)) {
                                            $val = '<a href="#" class="btn btn-primary btn-sm crudModal" data-url="'.$this->router->generate($getNameRoute.'_'.$this::PAGE.'_formtablist', ['id'=>$id ,'id_tab'=>$entity['id'], 'ongl'=>$ongl]).'" data-id="'.$entity['id'].'" data-callback="crudIhm.tabRefresh" data-title="Edition "><i class="fas fa-pencil"></i></a>';
                                        } else {
                                            $val = '';
                                        }

                                        break;
                                    default:
                                        break;
                                }
                            }
                            $row[$index] = $val;
                        }
                        $data[] = $row;
                    }
                }
            }
        }

        return $this->render('@Admin/tab.list.html.twig', [
            'page' =>$this::PAGE,
            'nameRoute' => $getNameRoute,
            'conf_onglet' => $onglets,
            'data_tab' => $data,
            'entityName' => $this->entityShortName
        ]);
    }

    /**
     * Fonction qui supprime un élément de la liste.
     *
     * @Route("delete/tab/{ongl}/form/{id_tab}")
     * @param $ongl
     * @param $id_tab
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteTabList($ongl, $id_tab, Request $request) {
        $jsonRep = new JsonResponse();
        $onglets = $this->getConfOnglets()->getConfArray();

        if (isset($onglets[$ongl]['entity']) && !empty($onglets[$ongl]['entity'])) {
            $entityTabName = $onglets[$ongl]['entity'];
            $thisEntity = $this->entityManager->getRepository($entityTabName)->find($id_tab);

            if ($this->isCsrfTokenValid('delete', $request->get("_token"))) {
                $this->entityManager->remove($thisEntity);
                $this->entityManager->flush();
            }
            return $jsonRep->setData(['data' => 1]);
        }
    }


    /**
     * Fonction de surcharge qui permet d'attribuer une valeur à une colonne dans datatable bien spécifique. (Doit être surchargée dans le controller voulu.)
     * Cette fonction de surcharge est bien spécifique à ce trait.
     *
     * @param $key
     * @param $entity
     * @param array $onglet
     * @return mixed|null
     */
    public function getColumnValueTab($key,  $entity, $onglet=[]){
        $return = null;

        if(isset($key) &&  isset($entity[$key])){
            $return = $entity[$key];
        }
        return $return;
    }
}