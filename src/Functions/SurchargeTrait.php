<?php


namespace emforbfc\AdminBundle\Functions;

Trait SurchargeTrait
{
    /*
     * Fonction de surcharge qui permet d'attribuer une valeur à une colonne dans datatable bien spécifique. (Doit être surchargée dans le controller voulu.)
     */
    protected function getColumnValue($key,  $entity, $options=[]){
        $return = null;

        if(isset($key) &&  isset($entity[$key])){
            $return = $entity[$key];
        }
        return $return;
    }

    /*
     * Fonction de surcharge qui permet de set un element bien spécifique dans editAction. (A surcharger dans le controller voulu).
     */
    protected function specificSetFunction($request, $objectManager, $entity) {
        return null;
    }

    /*
     * Fonction de surcharge des paramètres à envoyer au template Edit twig.
     */
    protected function getEditParam($entity) : array
    {
        return [];
    }

    /*
     * Fonction de surcharge des paramètres à envoyer au template New twig.
     */
    protected function getNewParam($instanceClass) : array {
        return [];
    }
}