<?php


namespace emforbfc\AdminBundle\Functions;


use PhpParser\Builder\Trait_;

Trait TwigTrait
{

    private $templateTwig = [
        'index'=> '@Admin/index.html.twig',
        'edit'=> '@Admin/edit.html.twig',
        'new'=> '@Admin/new.html.twig',
    ];

    /*
     * Initialisation basique du formulaire.
     */
    public function initializeForm($type, $instance, array $options = []) {
        return $this->createForm($type, $instance, $options);
    }

    /*
     * Récupérer le template twig à render.
     */
    public function getTemplateTwig($type){
        $templateName = null;
        if(!empty($this->templateTwig[$type])){
            $templateName = $this->templateTwig[$type];
        } else {
            throw new Exception('Template '.$type.' non défini');
        }
        return $templateName;
    }
    
    /*
     * Surcharge du template twig à render. (Doit être appelé dans le controller souhaité).
     */
    public function setTemplateTwig($type, $template){
        $this->templateTwig[$type] = $template;
    }
}